﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilletAutomat.Model
{
    public class BilletRepository
    {
        public double offenerBetrag { get; set; }

        public void SetOffenerBetrag(double offenerBetrag)
        {
            this.offenerBetrag = offenerBetrag;
        }

        public double GetOffenerBetrag()
        {
            return this.offenerBetrag;
        }

    }
}