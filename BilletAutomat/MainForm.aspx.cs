﻿using System;
using System.Threading;
using System.Web.UI;
using BilletAutomat.Model;

namespace BilletAutomat
{
    public partial class MainForm : System.Web.UI.Page
    {

        int statusOrt = 0;
        int statusKlasse = 0;
        int statusHinRetour = 0;
        double ticketBetrag = 0;

        BilletRepository myrepo = new BilletRepository();




        protected void Page_Load(object sender, EventArgs e)
        {
            //Page.isPostBack --> 
            if (Page.IsPostBack)
            {
                Response.Write("weiteres Mal");
                statusOrt = Convert.ToInt16(Session["Ort"]);
                statusKlasse = Convert.ToInt16(Session["Klasse"]);
                statusHinRetour = Convert.ToInt16(Session["HinRetour"]);
                ticketBetrag = Convert.ToDouble(Session["Betrag"]);

                if (statusOrt == 2 && statusKlasse > 0 && statusHinRetour > 0)
                {
                    double betrag = BetragBerechnen();
                    myrepo.SetOffenerBetrag(betrag);

                    BetragAnzeigen();
                    Session["Klasse"] = 0;
                    Session["HinRetour"] = 0;
                }
            }
            else
            {
                Response.Write("erstes Mal");
                
            }
            //string Session_ID;
            ////Ermitteln der Session_ID
            //Session_ID = Session.SessionID.ToString();

            ////Sessionvariable speichern (unschöner Programmierstil)
            //Session["ID"] = Session_ID;

            //Sessioncontrol
            if (Session.IsNewSession)
            {
                TextBox3.Text = Session.SessionID.ToString();
            }
        }



        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text == "")
            {
                TextBox1.Text = Button1.Text;
                ticketBetrag = ticketBetrag + 2;
                statusOrt++;

            }

            else if (TextBox2.Text == "")
            {
                TextBox2.Text = Button1.Text;
                statusOrt++;
                ticketBetrag = ticketBetrag + 2;
            }

            Session["Ort"] = statusOrt;
            Session["Betrag"] = ticketBetrag;

        }



        protected void Button2_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text == "")
            {
                TextBox1.Text = Button2.Text;
                ticketBetrag = ticketBetrag + 5;
                statusOrt++;
            }

            else if (TextBox2.Text == "")
            {
                TextBox2.Text = Button2.Text;
                ticketBetrag = ticketBetrag + 5;
                statusOrt++;
            }

            Session["Ort"] = statusOrt;
            Session["Betrag"] = ticketBetrag;

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text == "")
            {
                TextBox1.Text = Button3.Text;
                ticketBetrag = ticketBetrag + 7;
                statusOrt++;
            }

            else if (TextBox2.Text == "")
            {
                TextBox2.Text = Button3.Text;
                ticketBetrag = ticketBetrag + 7;
                statusOrt++;
            }

            Session["Ort"] = statusOrt;
            Session["Betrag"] = ticketBetrag;
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text == "")
            {
                TextBox1.Text = Button4.Text;
                ticketBetrag = ticketBetrag + 4;
                statusOrt++;
            }

            else if (TextBox2.Text == "")
            {
                TextBox2.Text = Button4.Text;
                ticketBetrag = ticketBetrag + 4;
                statusOrt++;
            }

            Session["Ort"] = statusOrt;
            Session["Betrag"] = ticketBetrag;
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text == "")
            {
                TextBox1.Text = Button5.Text;
                ticketBetrag = ticketBetrag + 2;
                statusOrt++;
            }

            else if (TextBox2.Text == "")
            {
                TextBox2.Text = Button5.Text;
                ticketBetrag = ticketBetrag + 2;
                statusOrt++;
            }

            Session["Ort"] = statusOrt;
            Session["Betrag"] = ticketBetrag;
        }

        protected void Button10_Click(object sender, EventArgs e)
        {
            TextBox1.Text = "";
            TextBox2.Text = "";
            statusOrt = 0;
        }



        protected void Button6_Click(object sender, EventArgs e)
        {
            //1.Klasse
            statusKlasse = 2;
            Session["Klasse"] = statusKlasse;
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            //2.Klasse
            statusKlasse = 1;
            Session["Klasse"] = statusKlasse;
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            //Hin
            statusHinRetour = 1;
            Session["HinRetour"] = statusHinRetour;
        }

        protected void Button9_Click(object sender, EventArgs e)
        {
            //Hin + Retour
            statusHinRetour = 2;
            Session["HinRetour"] = statusHinRetour;
        }


        /*Geldeinwurf*/

        protected void Button14_Click(object sender, EventArgs e)
        {
            RestBetragVerringern(2);
        }


        protected void Button11_Click(object sender, EventArgs e)
        {
            RestBetragVerringern(1);
        }

        protected void Button12_Click(object sender, EventArgs e)
        {
            RestBetragVerringern(0.50);
        }

        private double BetragBerechnen()
        {
            double offenerBetrag = ticketBetrag * statusKlasse * statusHinRetour;
            return offenerBetrag;
        }

        protected void Button13_Click(object sender, EventArgs e)
        {
            RestBetragVerringern(0.20);
        }

        //objekt für den Lock
        private object myLock = new object();

        private void BetragAnzeigen()
        {
            Label1.Text = myrepo.GetOffenerBetrag().ToString();
        }

        private void RestBetragVerringern(double einwurf)
        {
            Thread myThread = new Thread(BetragAnzeigen);
            RestBetragAnzeigen(einwurf * -1);
        }

        private void RestBetragAnzeigen(double v)
        {
            double offenerBetrag = myrepo.GetOffenerBetrag();
            offenerBetrag = offenerBetrag + (double)v;
            myrepo.SetOffenerBetrag(offenerBetrag);

            Label1.Text = offenerBetrag.ToString();
        }
    }
}