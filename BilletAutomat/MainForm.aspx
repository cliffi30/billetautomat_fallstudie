﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainForm.aspx.cs" Inherits="BilletAutomat.MainForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BilletAutomat</title>
    <style>
        .div-button {
            margin-left: 25px;
        }

        .button {
            background-color: goldenrod;
        }
        .button-geldeinwurf {
            background-color: coral;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
        <asp:Button ID="Button10" runat="server" OnClick="Button10_Click" Text="Reset" Style="background-color: red" />
        <p>
            Session ID
            <asp:TextBox ID="TextBox3" runat="server" Width="220px"></asp:TextBox>
        </p>
        <div>
            <asp:Button ID="Button1" runat="server" Text="Thun" class="button" OnClick="Button1_Click" />
            <asp:Button ID="Button2" runat="server" Text="Bern" class="div-button button" OnClick="Button2_Click" />
            <asp:Button ID="Button3" runat="server" Text="Zürich" class="div-button button" OnClick="Button3_Click" />
            <asp:Button ID="Button4" runat="server" Text="Biel" class="div-button button" OnClick="Button4_Click" />
            <asp:Button ID="Button5" runat="server" Text="Genf" class="div-button button" OnClick="Button5_Click" />
        </div>
        <hr />
        <p></p>
        <div>
            <asp:Button ID="Button6" runat="server" Text="1.Klasse" class="button" OnClick="Button6_Click" />
            <asp:Button ID="Button7" runat="server" Text="2.Klasse" class="div-button button" OnClick="Button7_Click" />
        </div>

        <hr />
        <p></p>
        <div>
            <asp:Button ID="Button8" runat="server" Text="Hin" class="button" OnClick="Button8_Click" />
            <asp:Button ID="Button9" runat="server" Text="Hin &amp; Retour" class="div-button button" OnClick="Button9_Click" />
        </div>

        <hr />
        <p>Abfahrtsort</p>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <p>Zielort</p>

        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>

        <hr />
        <h1>Restbetrag</h1>
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>

        <h2>Geldeinwurf</h2>
        <asp:Button ID="Button14" runat="server" Text="2 CHF" class="button-geldeinwurf" OnClick="Button14_Click" />
        <asp:Button ID="Button11" runat="server" Text="1 CHF" class="button-geldeinwurf div-button" OnClick="Button11_Click"/>
        <asp:Button ID="Button12" runat="server" Text="50 Rappen" class="button-geldeinwurf div-button" OnClick="Button12_Click"/>
        <asp:Button ID="Button13" runat="server" Text="20 Rappen" class="button-geldeinwurf div-button" OnClick="Button13_Click"/>


        <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>


    </form>
</body>
</html>
